package co.com.sophos.pruebatecnica.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class UtestRegistroPage {
    public static final Target REGISTER_BUTTON = Target.the("Button that shows us form to register")
            .located(By.xpath("//a[contains(text(), 'Join Today')]"));

    public static final Target FIRST_NAME = Target.the("Where do write the first name")
            .located(By.id("firstName"));

    public static final Target LAST_NAME = Target.the("Where do write the last name")
            .located(By.id("lastName"));

    public static final Target EMAIL = Target.the("Where do write the email")
            .located(By.id("email"));

    public static final Target MONTH = Target.the("Where do write the month")
            .located(By.id("birthMonth"));

    public static final Target DAY = Target.the("Where do write the day")
            .located(By.id("birthDay"));

    public static final Target YEAR = Target.the("Where do write the year")
            .located(By.id("birthYear"));

    public static final Target LANGUAGE = Target.the("Where do write the language")
            .located(By.xpath("//*[@id=\"languages\"]/div[1]/input"));

    public static final Target NEXT_BUTTON = Target.the("Button to confirm for continue the next form")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a/span"));

    public static final Target NEXT2_BUTTON = Target.the("Button to confirm for continue the next form")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/div/a/span"));

    public static final Target SELECT_MOBILE = Target.the("Select type device mobile")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[1]/div[2]/div/div[1]/span"));

    public static final Target SELECT2_MOBILE = Target.the("Select type device mobile")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[2]/div/input[1]"));

    public static final Target MODEL = Target.the("Select type MODEL")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[2]/div[2]/div/div[1]/span/span[1]"));

    public static final Target MODEL2 = Target.the("Select type MODEL")
            .located(By.xpath("/html/body/ui-view/main/section/div/div[2]/div/div[2]/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/input[1]"));

    public static final Target OPERATING_SYSTEM = Target.the("Select type operating system")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/div[1]/span"));

    public static final Target OPERATING_SYSTEM2 = Target.the("Select type operating system")
            .located(By.xpath("//*[@id=\"mobile-device\"]/div[3]/div[2]/div/input[1]"));

    public static final Target LAST_STEP = Target.the("Select type operating system")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/div[2]/div/a/span"));

    public static final Target PASSWORD = Target.the("Password")
            .located(By.id("password"));

    public static final Target CONFIRM_PASSWORD = Target.the("Confirm Password")
            .located(By.id("confirmPassword"));

    public static final Target TERMS = Target.the("Acept terms")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[5]/label/span[1]"));

    public static final Target PRIVACY = Target.the("Acept privacy")
            .located(By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[6]/label/span[1]"));

    public static final Target FINISH_BUTTON = Target.the("Select type operating system")
            .located(By.xpath("//a[contains(@class, 'btn btn-blue')]"));


}
