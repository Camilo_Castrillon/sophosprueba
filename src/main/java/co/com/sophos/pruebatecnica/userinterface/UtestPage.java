package co.com.sophos.pruebatecnica.userinterface;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.utest.com/")
public class UtestPage extends PageObject {
}
