package co.com.sophos.pruebatecnica.tasks;

import co.com.sophos.pruebatecnica.userinterface.UtestRegistroPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import org.openqa.selenium.Keys;

public class Registro implements Task {

    private String strFirstName;
    private String strLastName;
    private String strEmailAddress;
    private String strMonth;
    private String strDay;
    private String strYear;
    private String strlanguage;
    private String strSelectMobile;
    private String strModel;
    private String strOperatingSystem;
    private String strPassword;
    private String strConfirmPassword;

    public Registro(String strFirstName, String strLastName, String strEmailAddress, String strMonth,
                    String strDay, String strYear, String strlanguage, String strSelectMobile, String strModel,
                    String strOperatingSystem, String strPassword, String strConfirmPassword) {
        this.strFirstName = strFirstName;
        this.strLastName = strLastName;
        this.strEmailAddress = strEmailAddress;
        this.strMonth = strMonth;
        this.strDay = strDay;
        this.strYear = strYear;
        this.strlanguage = strlanguage;
        this.strSelectMobile = strSelectMobile;
        this.strModel = strModel;
        this.strOperatingSystem = strOperatingSystem;
        this.strPassword = strPassword;
        this.strConfirmPassword = strConfirmPassword;
    }



    public static Registro OnThePage(String strFirstName, String strLastName, String strEmailAddress, String strMonth,
                                     String strDay, String strYear, String strlanguage, String strSelectMobile,
                                     String strModel, String strOperatingSystem, String strPassword, String strConfirmPassword){
        return Tasks.instrumented(Registro.class,strFirstName,strLastName,strEmailAddress,strMonth,strDay,strYear,
                                    strlanguage, strSelectMobile, strModel, strOperatingSystem, strPassword, strConfirmPassword);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(UtestRegistroPage.REGISTER_BUTTON),
            Enter.theValue(strFirstName).into(UtestRegistroPage.FIRST_NAME),
            Enter.theValue(strLastName).into(UtestRegistroPage.LAST_NAME),
            Enter.theValue(strEmailAddress).into(UtestRegistroPage.EMAIL),
            SelectFromOptions.byVisibleText(strMonth).from(UtestRegistroPage.MONTH),
            SelectFromOptions.byVisibleText(strDay).from(UtestRegistroPage.DAY),
            SelectFromOptions.byVisibleText(strYear).from(UtestRegistroPage.YEAR),
            Enter.theValue(strlanguage).into(UtestRegistroPage.LANGUAGE),
            Click.on(UtestRegistroPage.NEXT_BUTTON)
        );
        actor.attemptsTo(
                Click.on(UtestRegistroPage.NEXT2_BUTTON),
                Click.on(UtestRegistroPage.SELECT_MOBILE),
                Enter.theValue(strSelectMobile).into(UtestRegistroPage.SELECT2_MOBILE).thenHit(Keys.ENTER),
                Click.on(UtestRegistroPage.MODEL),
                Enter.theValue(strModel).into(UtestRegistroPage.MODEL2).thenHit(Keys.ENTER),
                Click.on(UtestRegistroPage.OPERATING_SYSTEM),
                Enter.theValue(strOperatingSystem).into(UtestRegistroPage.OPERATING_SYSTEM2).thenHit(Keys.ENTER),
                Click.on(UtestRegistroPage.LAST_STEP),
                Enter.theValue(strPassword).into(UtestRegistroPage.PASSWORD),
                Enter.theValue(strConfirmPassword).into(UtestRegistroPage.CONFIRM_PASSWORD),
                Click.on(UtestRegistroPage.TERMS),
                Click.on(UtestRegistroPage.PRIVACY),
                Click.on(UtestRegistroPage.FINISH_BUTTON)

        )
        ;


    }
}
