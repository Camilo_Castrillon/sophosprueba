package co.com.sophos.pruebatecnica.stepdefinitions;

import co.com.sophos.pruebatecnica.model.UtestData;
import co.com.sophos.pruebatecnica.tasks.OpenPage;
import co.com.sophos.pruebatecnica.tasks.Registro;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class UtestStepDefinitions {

    @Before
    public  void SetStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^Than Cristian wants to learn automation at the utest$")
    public void thanCristianWantsToLearnAutomationAtTheUtest(List<UtestData> utestData) throws Exception {
        OnStage.theActorCalled("Cristian").wasAbleTo(OpenPage.ThePage(), Registro
                .OnThePage(utestData.get(0).getStrFirstName(), utestData.get(0).getStrLastName(),
                        utestData.get(0).getStrEmailAddress(),utestData.get(0).getStrMonth(),
                        utestData.get(0).getStrDay(),utestData.get(0).getStrYear(),utestData.get(0).getStrlanguage(),
                        utestData.get(0).getStrSelectMobile(),utestData.get(0).getStrModel(),utestData.get(0).getStrOperatingSystem(),
                        utestData.get(0).getStrPassword(),utestData.get(0).getStrConfirmPassword())
        );
    }

    @When("^He register a user on the utest platform$")
    public void heRegisterAUserOnTheUtestPlatform() {
    }

    @Then("^Register correctly$")
    public void registerCorrectly() {
    }
}
